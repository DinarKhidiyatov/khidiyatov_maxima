public interface Scalable {
    void increase(int count);
    void decrease(int count);
}
