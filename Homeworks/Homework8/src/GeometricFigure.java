public abstract class GeometricFigure implements Scalable, Relocatable{
    protected double area;
    protected double perimeter;
    protected int coordinateCentreX;
    protected int coordinateCentreY;
    protected double high;
    protected double width;

    public GeometricFigure(int coordinateCentreX, int coordinateCentreY, double high, double width) {
        this.coordinateCentreX = coordinateCentreX;
        this.coordinateCentreY = coordinateCentreY;
        this.high = high;
        this.width = width;
    }

    @Override
    public void increase(int count) {
        high *= count;
        width *= count;
    }

    @Override
    public void decrease(int count) {
        high /= count;
        width /= count;
    }

    @Override
    public void moveVertically(int step) {
        coordinateCentreX += step;
    }

    @Override
    public void moveHorizontally(int step) {
        coordinateCentreY += step;
    }

    public abstract void calculateArea();
    public abstract void calculatePerimeter();
}
