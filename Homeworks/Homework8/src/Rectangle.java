public class Rectangle extends GeometricFigure{

    public Rectangle(int coordinateCentreX, int coordinateCentreY, int high, int width) {
        super(coordinateCentreX, coordinateCentreY, high, width);
    }

    @Override
    public void calculateArea() {
        area = high * width;
    }

    @Override
    public void calculatePerimeter() {
        perimeter = 2 * (high + width);
    }
}
