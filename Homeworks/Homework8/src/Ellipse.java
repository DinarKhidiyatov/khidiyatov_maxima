public class Ellipse extends GeometricFigure{
    private double halfHigh;
    private double halfWidth;

    public Ellipse(int coordinateCentreX, int coordinateCentreY, double high, double width) {
        super(coordinateCentreX, coordinateCentreY, high, width);
        halfHigh = high / 2;
        halfWidth = width / 2;
    }

    @Override
    public void calculateArea() {
        area = Math.PI * halfHigh * halfWidth;
    }

    @Override
    public void calculatePerimeter() {
        perimeter = 4 * (Math.PI * halfHigh * halfWidth + (halfHigh - halfWidth)) / (halfHigh + halfWidth);
    }
}
