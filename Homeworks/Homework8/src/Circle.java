public class Circle extends Ellipse{

    public Circle(int coordinateCentreX, int coordinateCentreY, int radius) {
        super(coordinateCentreX, coordinateCentreY, radius, radius);
    }
}
