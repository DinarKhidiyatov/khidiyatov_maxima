public interface Relocatable {
    void moveVertically(int step);
    void moveHorizontally(int step);
}
