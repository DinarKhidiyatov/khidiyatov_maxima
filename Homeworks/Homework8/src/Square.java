public class Square extends Rectangle{

    public Square(int coordinateCentreX, int coordinateCentreY, int side) {
        super(coordinateCentreX, coordinateCentreY, side, side);
    }
}
