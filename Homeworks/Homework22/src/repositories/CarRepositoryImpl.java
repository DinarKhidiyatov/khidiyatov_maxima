package repositories;

import models.Car;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarRepositoryImpl implements CarRepository{

    String fileName;

    public CarRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> toCarMapper = string -> {
        String[] parsedLine = string.split("\\|");
        return new Car(parsedLine[0], parsedLine[1], parsedLine[2],
                Integer.parseInt(parsedLine[3]), Double.parseDouble(parsedLine[4]));
    };

    @Override
    public List<Car> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Car> findByColorOrMileage(String color, Integer mileage) {
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            return reader.lines().map(toCarMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage().equals(mileage))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
