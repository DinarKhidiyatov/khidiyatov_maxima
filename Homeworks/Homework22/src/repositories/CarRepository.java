package repositories;

import models.Car;

import java.util.List;

public interface CarRepository {
    List<Car> findAll();

    List<Car> findByColorOrMileage(String color, Integer mileage);
}
