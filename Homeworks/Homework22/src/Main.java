import models.Car;
import repositories.CarRepositoryImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        CarRepositoryImpl carRepository = new CarRepositoryImpl("input.txt");

        List<Car> byColorOrMileage = carRepository.findByColorOrMileage("Black", 0);
        byColorOrMileage
                .stream()
                .map(Car::getNumber)
                .forEach(System.out::println);

        List<Car> allCars = carRepository.findAll();

        //Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
        System.out.println(allCars
                .stream()
                .filter(car -> car.getPrice() > 700000 && car.getPrice() < 800000)
                .distinct()
                .count());

        //Вывести цвет автомобиля с минимальной стоимостью.
        System.out.println(allCars
                .stream()
                .min((c1, c2) -> c1.getPrice().compareTo(c2.getPrice())).get()
                .getColor());

        //Среднюю стоимость Camry
        System.out.println(allCars
                .stream()
                .filter(car -> car.getModel().equals("Camry"))
                .mapToDouble(Car::getPrice)
                .average()
                .getAsDouble());
    }
}
