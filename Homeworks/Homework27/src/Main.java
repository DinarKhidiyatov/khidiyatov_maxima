import models.Product;
import repositories.ProductsJdbcTemplateImpl;
import util.ProductRepositoryFactory;


public class Main {
    public static void main(String[] args) {

        ProductsJdbcTemplateImpl productsRepository = ProductRepositoryFactory.onJdbc();

        System.out.println(productsRepository.findAllByPrice(55.5).toString());

        System.out.println(productsRepository.findOneByName("Спагетти").toString());

//        Product product = Product.builder()
//                .productName("Шоколад")
//                .price(74.5)
//                .expirationDate(12)
//                .supplyDate("20220421")
//                .supplierName("Аленка")
//                .build();
//
//        productsRepository.save(product);
        Product milk = productsRepository.findOneByName("Молоко").get();
        milk.setPrice(80.34);
        productsRepository.update(milk);


        productsRepository.delete(milk);
    }
}
