package repositories;

import models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository {

    void save (Product product); // добавить новый товар

    List<Product> findAllByPrice(double price); // возвращает все товары одной стоимости

    Optional<Product> findOneByName(String name); // найти товар по названию

    void update(Product product); // обновить товар

    void delete(Product product); // удалить из базы данных
}
