package repositories;

import models.Product;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

public class ProductsJdbcTemplateImpl implements ProductsRepository {

    //language=sql
    private static final String SQL_INSERT_INTO_TABLE = "insert into " +
            "foodstuffs(product_name, price, expiration_date, supply_date, supplier_name) " +
            "values (?, ?, ?, ?, ?)";

    //language=sql
    private static final String SQL_SELECT_BY_PRICE = "select * from foodstuffs where price = ?";
    //language=sql
    private static final String SQL_SELECT_BY_NAME = "select * from foodstuffs where product_name = ?";
    //language=sql
    private static final String SQL_UPDATE_BY_ID = "update foodstuffs set product_name = ?," +
            " price = ?, expiration_date = ?, supplier_name = ?" +
            " where id = ?";
    //language=sql
    private static final String SQL_DELETE = "delete from foodstuffs where id = ?";

    private final JdbcTemplate jdbcTemplate;

    public ProductsJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .productName(row.getString("product_name"))
            .price(row.getDouble("price"))
            .expirationDate(row.getDouble("expiration_date"))
            .supplyDate(row.getString("supply_date"))
            .supplierName(row.getString("supplier_name"))
            .build();

    @Override
    public void save(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_TABLE,
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, product.getProductName());
            statement.setDouble(2, product.getPrice());
            statement.setDouble(3, product.getExpirationDate());
            statement.setString(4, product.getSupplyDate());
            statement.setString(5, product.getSupplierName());
            return statement;
        }, keyHolder);

        Long generatedId = ((Integer) keyHolder.getKeys().get("id")).longValue();
        product.setId(generatedId);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, productRowMapper, price);
    }

    @Override
    public Optional<Product> findOneByName(String name) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_NAME, productRowMapper, name));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        jdbcTemplate.update(SQL_UPDATE_BY_ID, product.getProductName(), product.getPrice(), product.getExpirationDate(),
                product.getSupplierName(), product.getId());
    }

    @Override
    public void delete(Product product) {
        jdbcTemplate.update(SQL_DELETE, product.getId());
    }
}
