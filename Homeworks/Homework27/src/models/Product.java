package models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Product {
    private Long id;
    private String productName;
    private double price;
    private double expirationDate;
    private String supplyDate;
    private String supplierName;
}
