drop table if exists foodstuffs;

create table foodstuffs
(
--     id int unique not null,
--     id serial unique not null,
    id         serial primary key,
    product_name varchar(20),
    --товары до 9999.99р., если не задано - 0, не может быть не задано
    price  DECIMAL(6,2) default 0 not null check (price > -1),
    --срок годности по месяцам
    expiration_date DECIMAL(3,1) check (expiration_date > 0),
    supply_date varchar(20),
    supplier_name varchar(20)
);
