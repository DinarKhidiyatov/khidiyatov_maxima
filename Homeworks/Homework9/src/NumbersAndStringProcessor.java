public class NumbersAndStringProcessor {
    private int[] numbers;
    private String[] strings;
    private int numbersCount;
    private int stringsCount;

    public NumbersAndStringProcessor(int[] numbers, String[] strings) {
        this.numbers = numbers;
        this.strings = strings;
        this.numbersCount = numbers.length;
        this.stringsCount = strings.length;
    }

    public String[] process(StringsProcess process){
        String[] processedArray = new String[stringsCount];
        for (int i = 0; i < stringsCount; i++) {
            processedArray[i] = process.process(strings[i]);
        }
        return processedArray;
    }

    public int[] process(NumbersProcess process){
        int[] processedArray = new int[numbersCount];
        for (int i = 0; i < numbers.length; i++) {
            processedArray[i] = process.process(numbers[i]);
        }
        return processedArray;
    }

    public int[] getNumbers() {
        return numbers;
    }
}
