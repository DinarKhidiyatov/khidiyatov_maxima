public class Main {
    public static void main(String[] args) {
        int[] numbers = {465012, 546052, 6650412, 70760643};
        String[] strings = {"Wi25nter", "Sp6rin7g", "Su1mm3er", "Au9tum0n"};

        NumbersProcess numbersProcess = number -> {
            int digit;
            int otherNumber = 0;
            while (number !=0) {
                digit = number % 10;
                number /= 10;
                if (digit == 0) {
                    continue;
                }
                if (digit % 2 == 1) {
                    digit--;
                }
                otherNumber *= 10;
                otherNumber += digit;
            }
            return otherNumber;
        };

        StringsProcess stringsProcess = process -> {
            char[] chars = process.toCharArray();
            int countChar = chars.length;
            int sizeNewChar = 0;
            char[] newChars = new char[countChar];
            for (int i = 0; i < countChar; i++) {
                if (Character.isDigit(chars[countChar - i - 1])){
                    continue;
                }
                newChars[sizeNewChar] = Character.toUpperCase(chars[countChar - i - 1]);
                sizeNewChar++;
            }
            return new String(newChars).trim();
        };

        NumbersAndStringProcessor processor = new NumbersAndStringProcessor(numbers, strings);

        int[] newArray = processor.process(numbersProcess);


        printInt(numbers);
        System.out.println();
        printInt(newArray);

        System.out.println();

        printString(strings);
        System.out.println();
        printString(processor.process(stringsProcess));
    }
    private static void printInt(int[] array){
        for (int number : array) {
            System.out.println(number);
        }
    }

    private static void printString(String[] array){
        for (String string : array) {
            System.out.println(string);
        }
    }
}
