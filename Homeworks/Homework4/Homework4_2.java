import java.util.Scanner;

class Homework4_2{
	
	public static int[] getArray(){
		Scanner scanner = new Scanner(System.in);
		int number;
		System.out.println("Введите количество чисел:");
		int lengthArray = scanner.nextInt();
		int[] array = new int[lengthArray];
		System.out.println("Введите числа:");
		for (int i = 0; i < lengthArray; i++){
			number = scanner.nextInt();
			if ((number > 999) || (number < -999)){
				System.out.println("Недопустимое значение. /n" + 
					"Будет записано: 0");
				continue;
			}
			array[i] = number;
		}
		return array;
	}	

	public static int findOutFrequentNumber(int[] array){
		int number = array[0];		
		int maxCountNumber = 1;
		for (int i = 0; i < array.length; i++){
			int countNumber = 0;
			for (int j = i; j < array.length; j++){
				if (array[i] == array[j])
					countNumber++;
			}
			if (countNumber >= maxCountNumber){
				maxCountNumber = countNumber;
				number = array[i];
			}
		}
		return number;
	}

	public static void main(String[] args) {
		int[] array = getArray();	
		int frequentNumber = findOutFrequentNumber(array);
		System.out.println("Число, которое встречается " + 
			"чаще остальных, это: " + frequentNumber);
	}
}
