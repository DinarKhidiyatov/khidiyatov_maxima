import java.util.Scanner;

class Homework4_1 {
	public static int parse (int array[]){	
		int number = 0;
		
		for (int i = 0; i < array.length; i++){
			number += array[i] * pow(10, array.length - 1 - i); 	
		}
		return number; 
	}

	public static int pow(int value, int powValue) {
		if (powValue == 1) {
			return value;
		}
		else {
			if (powValue == 0){
				return 1;
			}
			else{
				return value * pow(value, powValue - 1);
			}			
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int lengthArray = scanner.nextInt();
		int[] array = new int [lengthArray];
		
		for (int i = 0; i < lengthArray; i++){
			array[i] = scanner.nextInt();	
		}

		System.out.println(parse(array));
	}
}
