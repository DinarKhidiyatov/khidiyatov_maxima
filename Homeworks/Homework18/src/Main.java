import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        FileInputStream inputStream;

        try {
            inputStream = new FileInputStream("input.txt");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char[] characters;

        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;
        try {
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                // преобразуем байт в символ
                char character = (char) currentByte;
                // кидаем символ в массив
                characters[position] = character;
                position++;
                // считываем байт заново
                currentByte = inputStream.read();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        for (int i = 0; i < characters.length; i++) {
            if (characters[i] < 'A' ||
                    (characters[i] > 'Z' && characters[i] < 'a') ||
                    characters[i] > 'z') {
                characters[i] = ' ';
            }
        }

        String[] text = new String(characters).split("\\s");

        Map<String, Integer> counts = new HashMap<>();

        for (String word : text) {
            if (word.equals("")) {
                continue;
            }
            if (counts.containsKey(word)) {
                int count = counts.get(word);
                count++;
                counts.put(word, count);
            } else {
                counts.put(word, 1);
            }
        }

        int maxValue = 0;
        String string = null;
        Set<Map.Entry<String, Integer>> entrySet = counts.entrySet();
        for (Map.Entry<String, Integer> entry : entrySet) {
            if (entry.getValue() > maxValue) {
                string = entry.getKey();
                maxValue = entry.getValue();
            }
        }

        System.out.println(string + " - " + maxValue);
    }
}

