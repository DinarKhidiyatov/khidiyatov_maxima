import java.util.Scanner;

class Homework3 {
	
	static int minDigit (int number){
		int min = 10;	
		while (number != 0) {
			if (number % 10 < min)
				min = number % 10;
			number /= 10;
		}
		return min;
	}

	static boolean isEven (int number){
		return number % 2 == 0;	
	}

	static double getDigitAverage(int number){
		int sum = 0;
		int count = 0;
		while(number != 0){
			sum += number % 10;
			number /= 10;
			count++;
		}
		return (double)sum / count;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = 0;
		while (true) {
			number = scanner.nextInt();
			if (number == -1) break;
			if (isEven(number)) 
				System.out.println(minDigit(number));
			else 
				System.out.println(getDigitAverage(number));
		}	
	}
}
