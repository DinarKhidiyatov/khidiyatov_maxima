package ru.maxima.repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 29.01.2022
 * 25. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFileBasedImpl {

    private String fileName;

    public UsersRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    // возвращает список всех пользователей из файла
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while (reader.ready()) {
                String[] name = reader.readLine().split("\\|");
                User user = new User(name[0], name[1]);
                users.add(user);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return users;
    }

    public Optional<User> findByFirstName(String firstName) {
        List<User> users = findAll();
        for (User user : users) {
            if (firstName.equals(user.getFirstName())) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write(user.getFirstName() + "|" + user.getLastName());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
