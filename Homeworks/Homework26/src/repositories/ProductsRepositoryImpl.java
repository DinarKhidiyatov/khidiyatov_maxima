package repositories;

import models.Product;
import util.RowMapper;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductsRepositoryImpl implements ProductsRepository {
    
    //language=sql
    private static final String SQL_INSERT_INTO_TABLE = "insert into " +
            "foodstuffs(product_name, price, expiration_date, supply_date, supplier_name) " +
            "values (?, ?, ?, ?, ?)";
    //language=sql
    private static final String SQL_SELECT_BY_PRICE = "select * from foodstuffs where price = ?";
    //language=sql
    private static final String SQL_SELECT_BY_NAME = "select * from foodstuffs where product_name = ?";
    //language=sql
    private static final String SQL_UPDATE_BY_ID = "update foodstuffs set product_name = ?," +
            " price = ?, expiration_date = ?, supplier_name = ?" +
            " where id = ?";
    //language=sql
    private static final String SQL_DELETE = "delete from foodstuffs where id = ?";

    private final DataSource dataSource;

    public ProductsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final RowMapper<Product> productRowMapper = row -> new Product(
            row.getLong("id"),
            row.getString("product_name"),
            row.getDouble("price"),
            row.getDouble("expiration_date"),
            row.getString("supply_date"),
            row.getString("supplier_name"));

    @Override
    public void save(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_TABLE, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, product.getProductName());
            statement.setDouble(2, product.getPrice());
            statement.setDouble(3, product.getExpirationDate());
            statement.setString(4, product.getSupplyDate());
            statement.setString(5, product.getSupplierName());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert product");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (!generatedKeys.next()) {
                throw new SQLException("Can't retrieve generated id");
            }

            Long generatedId = generatedKeys.getLong("id");

            product.setId(generatedId);

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        List<Product> products = new ArrayList<>();

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_PRICE)) {

            statement.setDouble(1, price);

            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {
                    Product product = productRowMapper.mapRow(resultSet);
                    products.add(product);
                }
                return products;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Product> findOneByName(String name) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_NAME)) {

            statement.setString(1, name);

            try (ResultSet resultSet = statement.executeQuery()) {

                if (resultSet.next()) {
                    Product product = productRowMapper.mapRow(resultSet);

                    if (resultSet.next()) {
                        throw new SQLException("More than one rows");
                    }

                    return Optional.of(product);
                }

                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BY_ID)) {

            statement.setString(1, product.getProductName());
            statement.setDouble(2, product.getPrice());
            statement.setDouble(3, product.getExpirationDate());

            statement.setString(4, product.getSupplierName());
            statement.setLong(5, product.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update product");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Product product) {
        long id  = product.getId();

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE)) {

            statement.setLong(1, id);

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't delete product");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
