package util;

import repositories.ProductsRepositoryImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ProductRepositoryFactory {

    public static ProductsRepositoryImpl onJdbc() {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new DataSourceImpl(properties);
        return new ProductsRepositoryImpl(dataSource);
    }
}
