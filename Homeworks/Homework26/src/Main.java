import models.Product;
import repositories.ProductsRepositoryImpl;
import util.ProductRepositoryFactory;


public class Main {
    public static void main(String[] args) {

        ProductsRepositoryImpl productsRepository = ProductRepositoryFactory.onJdbc();

        System.out.println(productsRepository.findAllByPrice(55.5).toString());

        System.out.println(productsRepository.findOneByName("Спагетти").toString());

        Product milk = productsRepository.findOneByName("Молоко").get();
        milk.setPrice(80.34);
        productsRepository.update(milk);

        productsRepository.delete(milk);
    }
}
