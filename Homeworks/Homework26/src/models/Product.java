package models;

import java.util.Objects;
import java.util.StringJoiner;

public class Product {
    private Long id;
    private String productName;
    private double price;
    private double expirationDate;
    private String supplyDate;
    private String supplierName;

    public Product(Long id, String productName, double price, double expirationDate, String supplyDate, String supplierName) {
        this.id = id;
        this.productName = productName;
        this.price = price;
        this.expirationDate = expirationDate;
        this.supplyDate = supplyDate;
        this.supplierName = supplierName;
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public double getPrice() {
        return price;
    }

    public double getExpirationDate() {
        return expirationDate;
    }

    public String getSupplyDate() {
        return supplyDate;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Product.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("productName='" + productName + "'")
                .add("price=" + price)
                .add("expirationDate=" + expirationDate)
                .add("supplyDate='" + supplyDate + "'")
                .add("supplierName='" + supplierName + "'")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0
                && Double.compare(product.expirationDate, expirationDate) == 0
                && Objects.equals(id, product.id)
                && Objects.equals(productName, product.productName)
                && Objects.equals(supplyDate, product.supplyDate)
                && Objects.equals(supplierName, product.supplierName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productName, price, expirationDate, supplyDate, supplierName);
    }
}
