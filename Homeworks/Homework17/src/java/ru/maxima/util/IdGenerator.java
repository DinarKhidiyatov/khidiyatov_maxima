package ru.maxima.util;


public interface IdGenerator {
    int nextId();
}
