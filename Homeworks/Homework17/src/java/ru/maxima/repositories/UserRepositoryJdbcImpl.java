package ru.maxima.repositories;

import com.zaxxer.hikari.HikariDataSource;
import ru.maxima.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class UserRepositoryJdbcImpl implements UsersRepository {
    //language=sql
    private static final String SQL_INSERT =
            "insert into account (email, password) values (?, ?)";

    //language=sql
    private static final String SQL_FIND_ALL =
            "select * from account";

    //language=sql
    private static final String SQL_SELECT_BY_EMAIL =
            "select * from account where email = ?";

    //language=sql
    private static final String SQL_UPDATE_BY_ID =
            "update account set password = ? where id = ?";

    private final Function<ResultSet, User> rowToUserMapper = row -> {
        try {
            return new User(
                    row.getString("email"),
                    row.getString("password"));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    private final HikariDataSource dataSource;

    public UserRepositoryJdbcImpl() {
        dataSource = new HikariDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        dataSource.setUsername("postgres");
        dataSource.setPassword("qwerty007");
        dataSource.setMaximumPoolSize(20);
    }

    @Override
    public void save(User user) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT,
                     Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());

            int affectRows = statement.executeUpdate();
            if (affectRows != 1) {
                throw new SQLException("Сохранение не удалось");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (!generatedKeys.next()) {
                    throw new SQLException("Can't retrieve generated keys");
                }
                user.setId(generatedKeys.getInt("id"));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_EMAIL)) {
            statement.setString(1, email);
            try (ResultSet result = statement.executeQuery()) {
                if (!result.next()) {
                    return Optional.empty();
                }
                User user = rowToUserMapper.apply(result);
                user.setId(result.getInt("id"));
                return Optional.of(user);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet result = statement.executeQuery(SQL_FIND_ALL)) {
                while (result.next()) {
                    User user = rowToUserMapper.apply(result);
                    user.setId(result.getInt("id"));
                    users.add(user);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return users;
    }

    @Override
    public void update(User user, String pass) {
        user.setPassword(pass);
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BY_ID)) {

            statement.setString(1, user.getPassword());
            statement.setInt(2, user.getId());

            int affectRows = statement.executeUpdate();
            if (affectRows != 1) {
                throw new SQLException("Обновление не прошло");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
