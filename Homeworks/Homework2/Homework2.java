import java.util.Scanner;

class Homework2{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int countNumber = 0;
		for (int i = 1; i <= 10; i++){
			int number = scanner.nextInt();
			int sum = 0;
			while (number != 0){
				sum += number % 10;
				number /= 10;
			}	
			if (sum < 18) {
				countNumber++;
			}
		}
		System.out.println(countNumber);
	}
}
