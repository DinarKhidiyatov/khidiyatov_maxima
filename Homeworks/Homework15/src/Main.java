import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        try {
            BufferedReader reader1 = new BufferedReader(new FileReader("input.txt"));
            System.out.println("Номера всех автомобилей, имеющих черный цвет или нулевой пробег:");
            reader1.lines()
                    .map(s -> s.split(" "))
                    .filter(s -> (s[2].equals("black")) || (s[3].equals("0")))
                    .map(Arrays::toString)
                    .forEach(System.out::println);
            System.out.println();

            BufferedReader reader2 = new BufferedReader(new FileReader("input.txt"));
            System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.:");
            reader2.lines()
                    .map(s -> s.split(" "))
                    .distinct()
                    .filter(s -> (Integer.parseInt(s[4]) > 700000) && (Integer.parseInt(s[4]) < 800000))
                    .mapToInt(s -> s.length / 5)
                    .forEach(System.out::println);
            System.out.println();

            BufferedReader reader3 = new BufferedReader(new FileReader("input.txt"));
            System.out.println("Вывести цвет автомобиля с минимальной стоимостью:");
            System.out.println(reader3.lines()
                    .map(s -> s.split(" "))
                    .min(Comparator.comparing(s -> Integer.parseInt(s[4])))
                    .get()[2]);
            System.out.println();

            BufferedReader reader4 = new BufferedReader(new FileReader("input.txt"));
            System.out.println("Средняя стоимость Camry:");

            System.out.println(reader4.lines()
                    .map(s -> s.split(" "))
                    .filter(s -> s[1].equals("Camry"))
                    .mapToInt(s -> Integer.parseInt(s[4]))
                    .average()
                    .getAsDouble());


        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
