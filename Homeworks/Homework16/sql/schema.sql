-- select setval('account_id_seq', 12, true);

-- удаление таблицы, если она существует
drop table if exists account;
-- создание таблицы
create table account
(
    -- первичный ключ - семантика - уникальная запись (как правило числовая, как правило по принципу автоинкремента)
    -- обозначает столбец
    --  который однозначно идентифицирует строку
    id         serial primary key,
    email char(20),
    password  char(20)
);

-- добавление данных
insert into account(email, password)
values ('marsel@gmail.com', 'qwerty001');
insert into account(email, password)
values ('viktor@gmail.com', 'qwerty002');
insert into account(email, password)
values ('airat@gmail.com', 'qwerty003');
insert into account(email, password)
values ('maxim@@gmail.com', 'qwerty004');
insert into account(email, password)
values ('adelya@gmail.com', 'qwerty005');
insert into account(email, password)
values ('aliya@gmail.com', 'qwerty006');
insert into account(email, password)
values ('daniil@gmail.com', 'qwerty007');
insert into account(email, password)
values ('angelina@gmail.com', 'qwerty008');
insert into account(email, password)
values ('razil@gmail.com', 'qwerty009');
insert into account(email, password)
values ('timur@gmail.com', 'qwerty010');

-- удаление данных по условию
delete
from account
where id = 8;

-- добавление столбца с таблицу
alter table account
    add email char(30) unique;

-- обновление данных по условию
update account
set email = 'sidikov.marsel@gmail.com'
where id = 1;


-- получить все колонки всех записей таблицы account
select *
from account;
