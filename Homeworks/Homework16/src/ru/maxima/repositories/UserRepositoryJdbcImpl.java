package ru.maxima.repositories;

import ru.maxima.models.User;

import java.sql.*;
import java.util.List;
import java.util.Optional;

public class UserRepositoryJdbcImpl implements UsersRepository {
    //language=sql
    private static final String SQL_SELECT_BY_EMAIL = "select * from account where email = ?";

    @Override
    public void save(User user) {

    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                    "postgres", "qwerty007");
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_EMAIL);
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();

            if (!result.next()) {
                return Optional.empty();
            }
            User user = new User(result.getString("email"), result.getString("password"));
            user.setId(result.getInt("id"));
            return Optional.of(user);

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<User> findAll() {
        return null;
    }
}
