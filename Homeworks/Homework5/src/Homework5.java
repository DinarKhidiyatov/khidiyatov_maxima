import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Homework5 {

    //5.1
    public static int[] getArray(int arrayLength) {
        int[] array = new int[arrayLength];
        Random random = new Random();
        for (int i = 0; i < arrayLength; i++) {
            array[i] = random.nextInt();
        }
        return array;
    }

    //5.2
    public static void printAndDiff(int[] array1, int[] array2) {
        int diff = 0;
        for (int i = 0; i < array1.length; i++) {
            if (array1[1] == array2[i]) {
                continue;
            } else {
                diff = i;
                break;
            }
        }
        System.out.println(array1.toString() + "/n" + array2.toString() + "/n" +
                "Первый отличающийся елемент под номером: " + diff);
    }

    public static void copyArray(int[] array) {
        int[] changedArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            //да, что то я тут перемудрил))
            //if ((array[i] / 1000 == 0) && (array[i] % 1000 >= 100)) {
            if ((array[i] >= 100) && (array[i] <= 999)) {    // так будет проще)
                changedArray[i] = reverseNumber(array[i]);
                changedArray[i] += array[i];
            } else {
                changedArray[i] = array[i];
            }
        }
        printAndDiff(array, changedArray);
    }

    public static int reverseNumber(int number) {
        int reverse = 0;
        while (number != 0) {
            reverse *= 10;
            reverse += (number % 10);
            number /= 10;
        }
        return reverse;
    }

    //5.3
    public static int[][] getMatrix(int n, int m) {
        int[][] matrix = new int[n][m];
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }
        return matrix;
    }

    public static void sumOfElementsOnTheMainDiagonals(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            sum += matrix[i][i];
        }
        System.out.println("Сумма элементов на главной диагонали равна " + sum);
    }

    //5.4
    public static void changeArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длинну массива:");
        int arrayLength = scanner.nextInt();
        int[] array = getArray(arrayLength);
        int k = -1;
        while ((k < 0) || (k >= array.length)) {
            System.out.println("Введите k, не более " + (arrayLength - 1));
            k = scanner.nextInt();
        }
        array[k] = changeK(k);

        outputOddNumbers(array);
    }

    public static int changeK(int k) {
        int changedK = 0;
        for (int i = 0; i <= k; i++) {
            changedK *= 10;
            changedK += i;
        }
        return changedK;
    }

    public static void outputOddNumbers(int[] array) {
        System.out.println("Вывод нечетных чисел массива: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 1) {
                System.out.print(array[i] + " ");
            }
        }
    }

    //5.5
    public static void changeArrayTwo() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длинну массива:");
        int arrayLength = scanner.nextInt();
        int[] array = getArray(arrayLength);
        int k = -1;
        while ((k < 0) || (k >= array.length)) {
            System.out.println("Введите k, не более " + (arrayLength - 1));
            k = scanner.nextInt();
        }
        int l = -1;
        while ((l < 0) || (l >= array.length)) {
            System.out.println("Введите l, не более " + (arrayLength - 1));
            l = scanner.nextInt();
        }
        for (int i = 0; i < k; i++) {
            array[l + k] = 0;
        }
        printArray(array);
    }

    public static void printArray(int[] array) {
        System.out.println("Числа кратные 20:");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 20 == 0) {
                System.out.print(array[i] + " ");
            }
        }
    }

    public static void main(String[] args) {
    }
}
