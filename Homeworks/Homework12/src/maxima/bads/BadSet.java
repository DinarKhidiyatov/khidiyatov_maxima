package maxima.bads;

import maxima.HashMap;
import maxima.Iterator;
import maxima.Map;
import maxima.Set;

public class BadSet<T> implements Set<T> {

    private Map<T, Object> map = new HashMap<>();

    private static final Object PRESENT = new Object();

    @Override
    public void add(T element) {
        map.put(element, PRESENT);
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(T element) {
        return map.containsKey(element);
    }

    @Override
    public void remove(T element) {

    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}
