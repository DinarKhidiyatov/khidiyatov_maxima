package maxima.bads;

import maxima.ArrayList;
import maxima.Collection;
import maxima.Map;
import maxima.Set;

public class BadMap implements Map {

    private static final int MAX_ARRAY_SIZE = 31;

    public static class BadMapEntry implements Map.MapEntry {
        Object key;
        Object value;

        public BadMapEntry(Object key, Object value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public Object key() {
            return key;
        }

        @Override
        public Object value() {
            return value;
        }
    }

    private BadMapEntry[] entries;
    private int size;

    public BadMap() {
        this.entries = new BadMapEntry[MAX_ARRAY_SIZE];
    }

    @Override
    public void put(Object key, Object value) {
        for (int i = 0; i < size; i++) {
            // если уже есть значение с таким ключом
            if (entries[i].key.equals(key)) {
                // заменяем это значение
                entries[i].value = value;
                return;
            }
        }
        entries[size] = new BadMapEntry(key, value);
        size++;
    }

    @Override
    public Object get(Object key) {
        for (int i = 0; i < size; i++) {
            if (entries[i].key.equals(key)) {
                return entries[i].value;
            }
        }
        return null;
    }

    @Override
    public Set keySet() {
        // TODO: реализовать
        if (size == 0) {
            return null;
        }
        Set keys = new BadSet();
        for (int i = 0; i < size; i++) {
            keys.add(entries[i].key);
        }
        return keys;
    }

    @Override
    public Collection values() {
        // TODO: реализовать
        if (size == 0) {
            return null;
        }
        Collection values = new ArrayList();
        for (int i = 0; i < size; i++) {
            values.add(entries[i].value);
        }
        return values;
    }

    @Override
    public Set entrySet() {
        // TODO: реализовать
        if (size == 0) {
            return null;
        }
        Set entrySet = new BadSet();
        for (int i = 0; i < size; i++) {
            entrySet.add(entries[i]);
        }
        return entrySet;
    }

    @Override
    public boolean containsKey(Object key) {
        for (int i = 0; i < size; i++) {
            if (entries[i].key.equals(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        // TODO: реализовать
        if (size == 0) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (entries[i].value.equals(value)) {
                return true;
            }
        }
        return false;
    }
}
