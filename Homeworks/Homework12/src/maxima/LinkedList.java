package maxima;

public class LinkedList<F> implements List<F> {

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node<F> first;
    // ссылка на последний элемент
    private Node<F> last;

    private int size;

    @Override
    public F get(int index) {
        if (indexInBounds(index)) {
            // начинаем с первого узла
            Node<F> current = first;
            for (int i = 0; i < index; i++) {
                // сдвигаем current на нужную позицию
                current = current.next;
            }
            return current.value;
        }
        return null;
    }

    @Override
    public int indexOf(F element) {
        // TODO: реализовать
        if (contains(element)) {
            Node<F> current = first;
            int index = 0;
            while (!current.value.equals(element)) {
                current = current.next;
                index++;
            }
            return index;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(F element) {
        // TODO: реализовать
        if (contains(element)) {
            Node<F> current = first;
            int index = 0;
            for (int i = 0; i < size; i++) {
                if (current.value.equals(element)) {
                    index = i;
                }
                current = current.next;
            }
            return index;
        }
        return -1;
    }

    @Override
    public void add(F element) {
        // создали узел
        Node<F> newNode = new Node<>(element);
        // если список пустой
        if (isEmpty()) {
            // начало и конец - это единственный узел
            last = newNode;
            first = newNode;
        } else {
            // следующий элемент после последнего - новый узел
            last.next = newNode;
            // теперь новый узел является последним
            last = newNode;
        }
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(F element) {
        // TODO: реализовать
        if (!isEmpty()) {
            Node<F> current = first;
            for (int i = 0; i < size; i++) {
                if (current.value.equals(element)) {
                    return true;
                }
                current = current.next;
            }
        }
        return false;
    }

    @Override
    public void remove(F element) {
        // TODO: реализовать
        if (contains(element)) {
            Node<F> current = first;
            // проверяем первый элемент
            if (current.value.equals(element)) {
                first = current.next;
            } else {
                while (!current.next.value.equals(element)) {
                    current = current.next;
                }
                if (current.next.next == null) {
                    current.next = null;
                    last = current;
                } else {
                    current.next = current.next.next;
                }
            }
            size--;
        }
    }

    @Override
    public Iterator<F> iterator() {
        // TODO: реализовать
        return new LinkedListIterator();
    }

    private boolean indexInBounds(int index) {
        return index >= 0 && index < size;
    }

    private class LinkedListIterator implements Iterator<F> {

        private Node<F> current = first;
        private int index = 0;

        @Override
        public boolean hasNext() {
            return index < size;
        }

        @Override
        public F next() {
            if (!hasNext()) {
                return null;
            }
            F result = current.value;
            current = current.next;
            index++;
            return result;
        }
    }
}
