package maxima;

public class ArrayList<E> implements List<E> {

    // начальный размер массива
    private static final int INITIAL_SIZE = 10;
    // ссылка на массив для хранения элементов
    private E[] elements;
    // текущее количество элементов
    private int size;

    public ArrayList() {
        this.elements = (E[]) new Object[INITIAL_SIZE];
        this.size = 0;
    }

    @Override
    public E get(int index) {
        if (indexInBounds(index)) {
            return elements[index];
        }
        return null;
    }

    @Override
    public int indexOf(E element) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(E element) {
        // TODO: реализовать
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                index = i;
            }
        }
        return index;
    }

    @Override
    public void add(E element) {
        // если количество элементов равно размеру массива
        if (isOverhead()) {
            // изменяем размер
            resize();
        }
        // добавляем элемент в первую пустую позицию
        elements[size] = element;
        size++;
    }

    private void resize() {
        // создаем новый массив, его размер - это размер предыдущего увеличенный в полтора раза
        E[] newElements = (E[]) new Object[elements.length + elements.length / 2];

        // копируем элементы из старого массива в новый
        for (int i = 0; i < elements.length; i++) {
            newElements[i] = elements[i];
        }
        // затираем ссылку на старый массив ссылкой на новый массив, который в полтора раза больше
        this.elements = newElements;
    }

    private boolean isOverhead() {
        return size == elements.length;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        // TODO: реализовать
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void remove(E element) {
        // TODO: реализовать
        if (contains(element)) {
            E[] newElements = (E[]) new Object[elements.length - 1];
            int sizeNewElements = 0;
            for (E current : elements) {
                if (current.equals(element)) {
                    continue;
                }
                newElements[sizeNewElements] = current;
                sizeNewElements++;
            }
            elements = newElements;
            size--;
        }
    }

    private boolean indexInBounds(int index) {
        return index >= 0 && index < size;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    // внутренний класс, имеет доступ к полям объекта внешнего класса, например elements
    private class ArrayListIterator implements Iterator<E> {

        private int current = 0;

        @Override
        public boolean hasNext() {
            return current < size;
        }

        @Override
        public E next() {
            if (!hasNext()) {
                return null;
            }
            // запоминаю текущий элемент
            E result = elements[current];
            // двигаю курсор дальше
            current++;
            // возвращаю элемент, который я запомнил
            return result;
        }
    }
}
