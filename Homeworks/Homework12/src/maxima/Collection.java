package maxima;

public interface Collection<C> extends Iterable<C> {
    /**
     * Добавляет элемент в коллекцию
     * @param element добавляемый элемент
     */
    void add(C element);

    /**
     * Возвращает количество элементов в коллекции
     * @return целое число, равное количеству элементов
     */
    int size();

    /**
     * Проверяет, не является ли коллекция пустой
     * @return <code>true</code>, если в коллекции нет элементов, <code>false</code> в противном случае
     */
    boolean isEmpty();

    /**
     * Проверяет, содержит ли коллекция какой-либо элемент
     * @param element искомый элемент
     * @return <code>true</code> если элемент найден, <code>false</code> в противном случае
     */
    boolean contains(C element);

    /**
     * Удаляет указанный элемент
     * @param element удаляемый элемент
     */
    void remove(C element);


}