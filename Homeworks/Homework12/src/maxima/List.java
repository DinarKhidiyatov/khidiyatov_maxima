package maxima;

import maxima.Collection;

public interface List<D> extends Collection<D> {
    /**
     * Возвращает элемент под заданным индексом
     * @param index индекс элемента
     * @return возвращаемый элемент
     */
    D get(int index);

    /**
     * Возвращает индекс элемента, если он есть в списке (первый, который встретили)
     * @param element искомый элемент
     * @return индекс элемента, или <code>-1</code> если элемент не найден
     */
    int indexOf(D element);

    /**
     * Возвращает индекс элемента, если он есть в списке (последний, который встретили)
     * @param element искомый элемент
     * @return индекс элемента, или <code>-1</code> если элемент не найден
     */
    int lastIndexOf(D element);
}
