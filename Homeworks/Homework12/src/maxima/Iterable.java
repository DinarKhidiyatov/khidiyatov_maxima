package maxima;

public interface Iterable<B> {
    /**
     * Возвращает итератор по набору данных
     * @return объект-итератор
     */
    Iterator<B> iterator();
}