package maxima;

public interface Iterator<A> {
    /**
     * Проверяет, есть ли следующий элемент
     * @return <code>true</code>, если элементы еще есть, <code>false</code> в противном случае
     */
    boolean hasNext();

    /**
     * Возвращает следующий элемент и переводит курсор дальше
     * @return следующий на очереди элемент
     */
    A next();
}