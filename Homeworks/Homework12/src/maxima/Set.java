package maxima;

import maxima.bads.BadMap;

public interface Set<T> extends Collection<T> {
    @Override
    void add(T element);

    @Override
    int size();

    @Override
    boolean isEmpty();

    @Override
    boolean contains(T element);

    @Override
    void remove(T element);

    @Override
    Iterator<T> iterator();
}