package maxima;

import maxima.bads.BadSet;

public class HashMap<K, V> implements Map<K, V> {
    private static final int MAX_ARRAY_SIZE = 31;

    public static class MapEntry<K, V> implements Map.MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K key() {
            return key;
        }

        @Override
        public V value() {
            return value;
        }

        public MapEntry<K, V> getNext() {
            return next;
        }

        public void setNext(MapEntry<K, V> next) {
            this.next = next;
        }
    }

    private MapEntry<K, V>[] entries = new MapEntry[MAX_ARRAY_SIZE];

    private int size = 0;

    @Override
    public void put(K key, V value) {
        // получили хеш-код
        int hash = key.hashCode();
        // нужно уменьшить его до размеров массива
        int index = hash & entries.length - 1; // 0 .. 15

        // сейчас мы имеем индекс, куда хотим положить элемент, но там уже может лежать какой-то другой элемент
        // мы просто его положим туда же, после того элемента, который уже лежит (или до)
        // если в этом бакете ничего нет, то просто кладем туда элемент
        if (entries[index] == null) {
            entries[index] = new MapEntry<>(key, value);
        } else {
            // если в бакете уже лежат элементы
            // у нас два случая:
            // 1. у них просто совпал хеш-код/индекс
            // 2. мы кладем новое значение под уже существующим ключом

            // берем первый элемент бакета
            MapEntry<K, V> current = entries[index];

            // пока не просмотрим все элементы
            while (current != null) {
                // смотрим, не тот ли это ключ, который уже был?

                // сначала сравниваем хеш-коды, если они не совпадут, то понятное дело, это другой ключ
                // сравнение по хешам быстрее
                if (current.key.hashCode() == key.hashCode()) {
                    // если хеш-коды все-таки совпали, попробуем проверить по equals, разные ли это ключи?
                    if (current.key.equals(key)) {
                        // если они совпали по equals - то мы просто должны заменить значение, такой ключ уже был
                        current.value = value;
                        // останавливаем выполнение процедуры
                        return;
                    }
                    // если они не совпали по equals, то идем дальше
                }
                // если они не совпали по hashCode, то это разные ключи, идем дальше
                current = current.next;
            }
            // мы оказываемся здесь, если перебрали все ключи текущего бакета под index-ом
            // и не нашли такого же ключа
            // следовательно, нужно положить эту пару как новую в текуший бакет
            MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
            // следующий после нового узла - это первый узел, который был в бакете
            newMapEntry.next = entries[index];
            // теперь новый узел - первый в текущем бакете
            entries[index] = newMapEntry;
        }
        size++;
    }

    @Override
    public V get(K key) {
        // TODO: реализовать
        if (size == 0) {
            return null;
        }
        int index = key.hashCode() & entries.length - 1;
        MapEntry<K, V> current = entries[index];
        if (current.key.equals(key)) {
            return current.value;
        } else {
            while (current.next != null) {
                current = current.next;
                if (current.key.equals(key)) {
                    return current.value;
                }
            }
        }
        return null;
    }

    @Override
    public Set<K> keySet() {
        if (size == 0) {
            return null;
        }
        Set<K> keys = new BadSet<>();
        // перебираем все элементы массива, т.к. массив заполнялся не равномерно и могут быть пустоты (null)
        for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
            //если попали на пустую ячейку, то переходим к следуюущей
            if (entries[i] == null) {
                continue;
            }
            MapEntry<K, V> current = entries[i];
            keys.add(current.key);
            while (current.next != null) {
                keys.add(current.next.key);
                current = current.next;
            }
        }
        return keys;
    }

    @Override
    public Collection<V> values() {
        if (size == 0) {
            return null;
        }
        Collection<V> values = new ArrayList<>();
        for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
            if (entries[i] == null) {
                continue;
            }
            MapEntry<K, V> current = entries[i];
            values.add(current.value);
            while (current.next != null) {
                values.add(current.next.value);
                current = current.next;
            }
        }
        return values;
    }

    @Override
    public Set<Map.MapEntry<K, V>> entrySet() {
        if (size == 0) {
            return null;
        }
        Set<Map.MapEntry<K, V>> entrySet = new BadSet<>();
        for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
            if (entries[i] == null) {
                continue;
            }
            MapEntry<K, V> current = entries[i];
            entrySet.add(current);
            while (current.next != null) {
                entrySet.add(current.next);
                current = current.next;
            }
        }
        return entrySet;
    }

    @Override
    public boolean containsKey(K key) {
        if (size == 0) {
            return false;
        }
        int index = key.hashCode() & entries.length - 1;
        MapEntry<K, V> current = entries[index];
        if (current.key.equals(key)) {
            return true;
        }
        while (current.next != null) {
            if (current.next.key.equals(key)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public boolean containsValue(V value) {
        if (size == 0) {
            return false;
        }
        for (int i = 0; i < MAX_ARRAY_SIZE; i++) {
            MapEntry<K, V> current = entries[i];
            if (current.value.equals(value)) {
                return true;
            }
            while (current.next != null) {
                if (current.next.value.equals(value)) {
                    return true;
                }
                current = current.next;
            }
        }
        return false;
    }
}