import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TextConverter {
    public void toLowerCaseAll(String sourceFileName, String targetFileName) {
        FileInputStream inputStream;
        try {
            inputStream = new FileInputStream(sourceFileName);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }

        char[] chars;

        try {
            chars = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        int position = 0;
        try {
            int currentByte = inputStream.read();
            while (currentByte != -1) {
                char character = (char) currentByte;
                if (Character.isLetter(character) || character == ' ') {
                    chars[position++] = Character.toLowerCase(character);
                }
                currentByte = inputStream.read();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        //избавляемся от null-ов в старом массиве
        char[] newChars = new char[position + 1];
        for (int i = 0; i < newChars.length; i++) {
            newChars[i] = chars[i];
        }

        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(targetFileName);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
        byte[] bytes = new String(newChars).getBytes();
        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
