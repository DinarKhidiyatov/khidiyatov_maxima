public class Main {
    public static void main(String[] args) {
        BadStringBuilder builder = new BadStringBuilder();
        builder.append("Привет!")
                .append(" ")
                .append("Как дела?");
        System.out.println(builder.toString());
    }
}
