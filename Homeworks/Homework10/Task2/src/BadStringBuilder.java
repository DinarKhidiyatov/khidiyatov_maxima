public class BadStringBuilder {
    private char[] chars;
    private int size;

    public BadStringBuilder() {
        size = 0;
    }

    public BadStringBuilder append(String string) {
        int newSize = size + string.length();
        char[] newChars = new char[newSize];
        if (chars == null){
            chars = new char[1];
        }
        System.arraycopy(chars, 0, newChars, 0, size);

        char[] stringArray = string.toCharArray();

        int k = 0;
        for (int i = size; i < newSize; i++) {
            newChars[i] = stringArray[k];
            k++;
        }
        chars = newChars;
        size = newSize;
        return this;
    }

    @Override
    public String toString() {
        return new String(chars);
    }
}
