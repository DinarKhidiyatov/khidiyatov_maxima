public class Human {
    private String name;
    private int age;
    private boolean maritalStatus;
    private char firstLetterOfLastName;
    private boolean haveChildren;

    public Human(String name, char firstLetterOfLastName, int age, boolean maritalStatus, boolean haveChildren) {
        this.name = name;
        this.age = age;
        this.maritalStatus = maritalStatus;
        this.firstLetterOfLastName = firstLetterOfLastName;
        this.haveChildren = haveChildren;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Human)) {
            return false;
        }
        Human that = (Human) object;
        return this.name == that.name && this.age == that.age && this.maritalStatus == that.maritalStatus
                && this.firstLetterOfLastName == that.firstLetterOfLastName
                && this.haveChildren == that.haveChildren;
    }

    @Override
    public String toString() {
        return "Информация о человеке: \n"
                + "Имя - " + name + " " + firstLetterOfLastName + ".\n"
                + "Возраст - " + age + "\n"
                + "Семейное положение - " + (maritalStatus ? "женат/замужем" : "не нагулялся(сь)") + "\n"
                + "Наличие детей - " + (haveChildren ? "имеются" : "не нагулялся(сь)") + "\n";
    }
}
