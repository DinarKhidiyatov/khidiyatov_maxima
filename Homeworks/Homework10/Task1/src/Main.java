public class Main {
    public static void main(String[] args) {
        Human h1 = new Human("Генадий", 'К', 31, true, true);
        Human h2 = new Human("Генадий", 'К', 31, true, true);
        Human h3 = new Human("Генадий", 'К', 31, true, true);
        Human h4 = new Human("Генадий", 'К', 31, true, true);
        Human h5 = new Human("Генадий", 'К', 31, true, true);

        System.out.println("task1.EqualsUtil - " + EqualsUtil.isEquals(h1, h2, h3, h4, h5));

        Human h6 = new Human("Кристина", 'А', 26, false, false);
        System.out.println(h1);
        System.out.println(h6);

        System.out.println("task1.EqualsUtil - " + EqualsUtil.isEquals(h1, h6));
    }
}
