public class EqualsUtil {
    public static boolean isEquals(Object ... object) {
        for (int i = 0; i < object.length - 1; i++) {
            if (!object[i].equals(object[i+1])) {
                return false;
            }
        }
        return true;
    }
}
