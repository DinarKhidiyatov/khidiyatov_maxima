--Получить товары, которые дороже 100 рублей
select * from foodstuffs where price > 100;
--Получить товары, которые были поставлены ранее 02-02-2020 года
select * from foodstuffs where supply_date < '20200202';
--Получить названия всех поставщиков *
select supplier_name from foodstuffs;