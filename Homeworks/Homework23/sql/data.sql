insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Молоко', 55.5, 0.2, '20220222', 'Веселая коровка') ;
insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Хлеб', 38.5, 0.2, '20220222', 'Хлебозавод') ;
insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Вода', 33.66, 6, '20220122', 'Святой источник') ;
insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Сыр', 80.34, 1, '20220222', 'Рокфор') ;
insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Яйца', 84.5, 1, '20220222', 'Курочка Ряба') ;
insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Спагетти', 133.33, 6, '20220115', 'Дон Карлеоне') ;
insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Чай', 159.15, 24, '20220101', 'Чай, индийский чай') ;
insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Рис', 83.47, 12, '20220110', 'Прямиком из Китая') ;
insert into foodstuffs(product_name, price, supply_date, supplier_name)
values ('Соль', 55.5, '20200522', 'Сольилецк') ;
insert into foodstuffs(product_name, price, expiration_date, supply_date, supplier_name)
values ('Черная икра', 3412.99, 2, '20220214', 'Дары моря') ;