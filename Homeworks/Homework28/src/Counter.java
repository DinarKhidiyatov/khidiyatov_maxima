public class Counter extends Thread{
    private final int from;
    private final int to;
    private final int[] array;

    public Counter(int from, int to, int[] array) {
        this.from = from;
        this.to = to;
        this.array = array;
    }

    @Override
    public void run() {
        // TODO: сделать
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += array[i];
        }
        System.out.println(Thread.currentThread().getName() + " - Сумма от " + from + " до " + to + " = " + sum);
    }
}
