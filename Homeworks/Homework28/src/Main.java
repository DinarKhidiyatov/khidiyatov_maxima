import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int numbersCount = scanner.nextInt();
        int threadsCount = scanner.nextInt();

        int[] array = new int[numbersCount];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000);
        }

        int step = array.length / threadsCount;
        int from = 0;
        int to = from + step - 1;

        for (int i = 1; i <= threadsCount; i++) {
            if (i == threadsCount && to != array.length - 1) {
                to = array.length - 1;
            }
            new Counter(from, to, array).start();
            from += step;
            to += step;
        }

        int sum = 0;

        for (int number : array) {
            sum += number;
        }

        System.out.println(sum);
    }
}
