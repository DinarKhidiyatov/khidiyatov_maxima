public class Channel {
    private String name;
    private Program[] programs;

    public Channel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPrograms(Program[] programs) {
        this.programs = programs;
    }

    public Program[] getPrograms() {
        return programs;
    }
}

