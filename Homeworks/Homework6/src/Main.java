public class Main {
    public static void main(String[] args) {
        TV tv = new TV("Гостинная", "Sony");
        tv.setChannels(new Channel[]{
                new Channel("Первый"),
                new Channel("Россия"),
                new Channel("Культура"),
                new Channel("Пятница"),
                new Channel("МузТВ")
        });
        Channel channel1 = tv.getChannels()[0];
        channel1.setPrograms(new Program[]{
                new Program("Время"),
                new Program("Новости"),
                new Program("Сериал"),
                new Program("Новости Сериала"),
                new Program("Время покажет"),
                new Program("Время увидит")
        });
        tv.getRemoteController().on(0);
        //Вводить программы по аналогии. Развлекайтесь:)
    }
}
