import java.util.Random;

public class TV {

    public class RemoteController {
        private String model;

        private RemoteController(String model) {
            this.model = model;
        }

        public void on(int channelNumber) {
            if (indexInBounds(channelNumber)) {
                Random random = new Random();
                Channel channel = getChannels()[channelNumber];
                Program[] programs = channel.getPrograms();
                Program program = programs[random.nextInt(programs.length)];

                System.out.println("На канале " + channel.getName() + " идет программа " + program.getName());
            }
            else System.err.println("Канал не найден");
        }
    }

    private String name;
    private String model;
    private RemoteController remoteController;
    private Channel[] channels;

    public TV(String name, String model) {
        this.name = name;
        this.model = model;
        this.remoteController = new RemoteController(model);
    }

    public void setChannels(Channel[] channels) {
        this.channels = channels;
    }

    public Channel[] getChannels() {
        return channels;
    }

    public RemoteController getRemoteController() {
        return remoteController;
    }

    private boolean indexInBounds(int index){
        return index >= 0 && index < channels.length;
    }
}
