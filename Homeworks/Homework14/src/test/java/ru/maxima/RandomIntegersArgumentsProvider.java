package ru.maxima;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class RandomIntegersArgumentsProvider implements ArgumentsProvider {

    private Random random = new Random();

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        List<Arguments> numbers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            int number = random.nextInt();
            Arguments argument = Arguments.of(String.valueOf(number));
            numbers.add(argument);
        }
        return numbers.stream();
    }
}
