package ru.maxima.repositories;

import ru.maxima.models.User;
import ru.maxima.repositories.exceptions.UniqueEmailException;

import java.util.Optional;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersRepository {
    void save(User user);

    Optional<User> findByEmail(String email);

    boolean uniqueEmail(String email) throws UniqueEmailException ;
}
