package ru.maxima;

import ru.maxima.repositories.UsersRepository;
import ru.maxima.repositories.UsersRepositoryFilesImpl;
import ru.maxima.services.UsersServiceImpl;
import ru.maxima.util.IdGenerator;
import ru.maxima.util.IdGeneratorFilesImpl;
import ru.maxima.validators.EmailFormatValidator;
import ru.maxima.validators.EmailValidator;
import ru.maxima.validators.PasswordLengthValidator;
import ru.maxima.validators.PasswordValidator;

public class Main {

    public static void main(String[] args) {
//        UsersRepository usersRepository = new UsersRepositoryFakeImpl();
        IdGenerator idGenerator = new IdGeneratorFilesImpl("users_id_sequence.txt");
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt", idGenerator);
        EmailValidator emailValidator = new EmailFormatValidator();
        PasswordValidator passwordValidator = new PasswordLengthValidator();
        UsersServiceImpl usersService = new UsersServiceImpl(usersRepository, emailValidator, passwordValidator);
//        usersService.signIn("sidikov.marsel@gmail.com", "qwerty007");
        usersService.signIn("user3@gmail.com", "qwerty009");

        usersService.signUp("user1@gmail.com", "qwerty007");
    }
}
